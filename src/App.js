import React from 'react';
import { Router } from "@reach/router";
import './App.css';
import Frame from './Components/Frame';
import Endless from './Components/Endless';
import Swirl from './Components/Swirl';
import Twisted from './Components/Twisted';
import Relax from './Components/Relax';

function App() {
  return (
    <>
			<Frame />
      <Router>
        <Endless path="/endless" default />
        <Swirl path="/swirl" />
        <Twisted path="/twisted" />
        <Relax path="/relax" />
      </Router>
		</>
  );
}

export default App;
