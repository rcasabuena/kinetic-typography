import React, { Component } from 'react';

class Title extends Component {
  render() {
    return (
      <div className="frame__title-wrap">
        <h1 className="frame__title">Kinetic Typography with Three.js</h1>
      </div>
    );
  };
};

export default Title;
