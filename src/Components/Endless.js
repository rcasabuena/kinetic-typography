import React, { Component } from 'react';
import './css/Endless.css';
import { withOptions } from './hoc';
import { setDemoClass, renderType } from './utils';

class Endless extends Component {

  constructor(props) {
    super(props);
    this.renderType = renderType.bind(this);
  }

  componentDidMount() {
    const { items, item } = this.props;
    setDemoClass(items, item);
    this.renderType();
  }

  render() {
    const { item } = this.props;
    return (
      <div className="content">
        <h1>{item.name}</h1>
        <div id="webgl"></div>
			</div>
    );
  };
};

export default withOptions(Endless, 'endless');