import React, { Component } from 'react';

class Links extends Component {
  render() {
    return (
      <div className="frame__links">
        <a href="https://bitbucket.org/rcasabuena/kinetic-typography" target="_blank">Bitbucket</a>
      </div>
    );
  };
};

export default Links;