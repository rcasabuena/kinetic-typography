import { connect } from 'react-redux' 
import { getItem, getItems } from '../utils';

export const withOptions = (Page, item) => {
  
  const mapStateToProps = state => {
    return {
      item: getItem(state, item),
      items: getItems(state)
    };
  };

  return connect(mapStateToProps)(Page);

}