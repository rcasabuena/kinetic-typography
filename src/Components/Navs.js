import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "@reach/router";
import { getItems } from './utils';

class Navs extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    const { items } = this.props;

    return (
      <div className="frame__demos">
        {items.map(item => <Link key={item.name} getProps={({isCurrent, location}) => {
          return ({className: isCurrent || (location.pathname == "/" && item.default) ? "frame__demo frame__demo--current" : "frame__demo"});
        }} to={item.class}>{item.name}</Link>)}
      </div>
    );
  };
};

const mapStateToProps = state => {
  return {
    items: getItems(state),
  }
}

export default connect(mapStateToProps)(Navs);