import React, { Component } from 'react';
import './css/Swirl.css';
import { withOptions } from './hoc';
import { setDemoClass, renderType } from './utils';

class Swirl extends Component {

  constructor(props) {
    super(props);
    this.renderType = renderType.bind(this);
  }

  componentDidMount() {
    const { items, item } = this.props;
    setDemoClass(items, item);
    this.renderType();
  }

  render() {
    return (
      <div className="content">
        <h1>Swirl</h1>
        <div id="webgl"></div>
			</div>
    );
  };
};

export default withOptions(Swirl, 'swirl');