import React, { Component } from 'react';
import './css/Twisted.css';
import { withOptions } from './hoc';
import { setDemoClass, renderType } from './utils';

class Twisted extends Component {

  constructor(props) {
    super(props);
    this.renderType = renderType.bind(this);
  }

  componentDidMount() {
    const { items, item } = this.props;
    setDemoClass(items, item);
    this.renderType();
  }

  render() {
    return (
      <div className="content">
        <h1>Twisted</h1>
        <div id="webgl"></div>
			</div>
    );
  };
};

export default withOptions(Twisted, 'twisted');