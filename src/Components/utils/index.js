export const setDemoClass = (items, item) => {
  let docBody = document.body;
  items.forEach(item => docBody.classList.remove(item.class));
  docBody.classList.add(item.class);
}

export const getItems = ({items}) => {
  return items;
}

export const getItem = ({items}, name) => {
  return items.find(item => item.class === name);
}

export function renderType() {
  console.log(this);
}