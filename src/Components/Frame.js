import React, { Component } from 'react';
import Title from './Title';
import Links from './Links';
import Navs from './Navs';

class Frame extends Component {
  render() {
    return (
      <div className="frame">
				<Title />
				<Links />
				<Navs />
			</div>
    );
  };
};

export default Frame;